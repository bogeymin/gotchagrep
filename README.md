# gotchagrep

## Overview

A command line tool for extracting "gotchas" from text files. 

## Usage

> Note: See `gotchagrep --help` for full usage options.

Consider this comment from a fictitious Python source code file called
`kludge.py`.

    # REFACTOR: This whole section could be replaced with 3 lines of code.

Then use `gotchagrep`:

    gotchagrep -e py -g REFACTOR:

This will result in output that looks something like this:

    ========
    REFACTOR
    ========

    kludge.py
    ---------

    This whole section could be replaced with 3 lines of code.

## Installation

Installation is simple:

    make install;

This will install `gotchagrep` into `/opt/bin` by default. To change
this, simply:

    make install PREFIX=/path/to/my/preferred/bin

## Matching Rules

- You gotcha must **always** start on a new line. `gotchagrep` looks for the
  *leading marker* and *gotcha string* at the beginning of the line.
    - The gotacha string defaults to `TODO:`.
    - The leader defaults to `# ` (with a space), but may be changed using the
      `-L` switch. When specifying your own leader, a common mistake is to
      forget the space.
    - So, by default, `# TODO:` is what is matched.
- Multiple lines are supported. All subsequent new lines will be included in
  the match until an empty line is encountered.
- If you have two gotchas, one after the other, make sure there is a blank line
  in between them. Otherwise, they will be matched as a single gotcha.

## New in this Version

- First steps toward generating proper ReStructuredText output for text format.
- Better handling of printing the file name in text format.
- Output is organized by file.
- Killing the script with CTRL + C no longer results in ugly errors.

## Ideas and To-dos

- Facilitate including and capturing some basic meta data with each gotcha.
    - Meta data should be contained in [brackets].
    - Generally speaking, meta data should be placed at the end of the gotcha.
      However, if it makes sense for the data to be within the gotcha text, then
      place it there instead.
    - Line wraps should never occur within the brackets.
    - Separate multiple meta data entries with a | pipe.
    - Define various standard codes, such as:
        - a: <author name, email or initials>
        - d: <date YYYY-MM-DD>
        - e: <estimated hours>
        - t: <ticket_id>
    - Examples:
        - QUESTION: Should measures be tied to objectives or goals? [a: Bob].
        - NOTE: As of [d: 2012-09-12] there is a bug with the compiler that
          causes unexpected output.
        - TODO: Implement the connection between the local database and the
          remote API. [e: 4]
        - BUG: Clean up this area of the code in response to [t: 1234].
- Facilitate including and capturing tags within the gotcha.
    - Tags are enclosed in {curly brackets}.
    - Tags may be only a single word.
    - Tags must only appear at the end of the gotcha.
    - Multiple tags are separated by a comma.
    - Examples:
        - BUG: This form renders very badly in IE. {design}
        - QUESTION: What are the memory requirements for this third-party
          module? {admin}
        - TODO: Implement the connection between the local database and the
          remote API. {programming}
- HTML output could certainly be improved.
- Better handling of printing the file name in HTML format.
- CSV output format.
