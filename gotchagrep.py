#! /usr/bin/env python

"""
Grep for TODO in an area of code and display them back.

OPTIONS

-e <extension>
    The file extension to look in. Defaults to *.*

-f <format>
    Format of the output; "text" (the default), or "html".

--file-decorator=<character>
    Files are represented as sub-headings in the text output. The default
    decorator is "-".

-g <gotcha>
    The string to be found. Defaults to "TODO:"

-h
    Print help and exit.

--help
    Print more help and exit.

-L <leader>
    The leading character or characters for the gotcha. Defaults to "# ".

-p <path>
    Path to the code in question. Defaults to '.'

-t <title>
    Optional title for the output. Defaults to the gotcha name above.

--title-decorator=<character>
    The character to use for the title decoration in text-only output. Defaults
    to "=".

-v
    Print version and exit.

--version
    Print verbose version info and exit.

"""

# Python Imports
import commands
import getopt
import os
import re
import sys

# Administrivia
__author__ = "F.S. Davis <consulting@fsdavis.com>"
__command__ = os.path.basename(sys.argv[0])
__date__ = "2012-09-12"
__version__ = "0.3.0-d"

# Functions

def main():
    """Find gotchas and print them out."""

    # Capture options and arguments.
    try:
        options, arguments = getopt.getopt(sys.argv[1:], "e:f:g:hL:p:t:v", ["help", "file-decorator=", "title-decorator=", "version"])
    except getopt.GetoptError, err:
        print str(err)
        sys.exit(1)

    # Set defaults.
    extension = "*"
    file_decorator = '-'
    format = "text"
    gotcha = 'TODO:'
    leader = "# "
    path = '.'
    title = None
    title_decorator = '='

    # Parse options.
    for name, value in options:
        if '-h' == name:
            print main.__doc__
            sys.exit(0)
        elif '--help' == name:
            print __doc__
            sys.exit(0)
        elif '-v' == name:
            print __version__
            sys.exit(0)
        elif '--version' == name:
            print "%s %s %s" %(__command__, __version__, __date__)
        elif '-e' == name:
            extension = value
        elif '-f' == name:
            format = value
        elif '--file-decorator' == name:
            file_decorator = value
        elif '-g' == name:
            gotcha = value
        elif '-L' == name:
            leader = value
        elif '-p' == name:
            path = value
        elif '-t' == name:
            title = value
        elif '--title-decorator' == name:
            title_decorator = value
        else:
            assert False, "Unhandled option: %s" %(name)

    # The title should default to the gotcha string.
    if title is None:
        title = gotcha
    title_decorator_length = len(title)

    # Get a list of files. This seemed to be easiest to do by calling find and
    # splitting the results.
    (status, output) = commands.getstatusoutput('find %s -type f -name "*.%s"' % (path, extension))
    files = output.split('\n')

    # Run the processing within a try to catch keyboard interrupts.
    try:
        # Check each file for the desired gotcha.
        pattern = "^%s%s" % (leader, gotcha)
        matches = list()
        for f in files:

            # Again, it seemed easiest to let grep determine if there is a match in
            # each file. (Why not use grep? It doesn't support newlines.) I tried
            # using Python's regex to obtain matches from within # the content
            # string, but it never quite worked as I expected. So grep is used to
            # know if the file contains a match, and a simple line-by-line review
            # is used to capture multi-line gotchas.
            (status, output) = commands.getstatusoutput('grep "%s" %s' % (gotcha, f))
            if status == 0:
                h = open(f, 'rb')
                content = h.read().split("\n")
                h.close()

                file_decorator_length = len(f)
                matches.append((f, file_decorator * file_decorator_length))

                start = False
                for line in content:
                    line_stripped = line.lstrip()
                    if re.match(pattern, line_stripped):
                        start = True
                        match = list()
                        #match.append(f)
                        match.append(line_stripped)
                    elif start == True:
                        if len(line) == 0:
                            start = False
                            matches.append(match)
                        else:
                            match.append(line)
                    else:
                        pass
    except KeyboardInterrupt:
        sys.exit()

    # Give up if there were no matches.
    if len(matches) == 0:
        print "There are no matches for gotcha", gotcha
        sys.exit(5)

    # Output the header of the report.
    if format == 'html':
        print wrap_html("h1", title)
        print "<ul>"
    else:
        print ""
        #print "=" * 79
        print title
        print title_decorator * title_decorator_length
        print ""

    # Print each match in the appropriate format.
    for m in matches:
        s = "\n".join(m)
        s = s.replace(gotcha, "")
        s = s.replace(leader+" ", "")
        s = s.replace(leader, "")

        if format == "html":
            print "<li>%s</li>" % (s)
        else:
            print s
            print ""

    if format == "html":
        print "</ul>"

    # Hush now.
    sys.exit()

def wrap_html(tag, content):
    """Shortcut for putting content into an HTML tag."""
    return "<%s>%s</%s>" % (tag, content, tag)

# Kickoff
if __name__ == "__main__":
	main()

